## DXF cleaner
#  this is a tool that takes DXF files (generated by Kicad) and looks at entpoints of ARCs and LINEs
#  if the endpoints are within a given tolerance the endpoints of the LINEs are placed at the exact same coordinate
#  as the endpoint of the ARC. ARCs points are not moved – only the connecting LINEs.
#  CIRCLEs are left alone completely

# author: AnSc
# date: 2023-06-24
# requires: ezdxf (depends on pyparser and typing_extensions)

import ezdxf
import math
import matplotlib as mpl
formatter = mpl.ticker.EngFormatter()

filepath = r"D:\\"
infile  = filepath+r"infile.dxf"
outfile = filepath+r"outfile.dxf"
logfile = filepath+r"corrections.txt"

base_unit = 0.001 ## base unit is mm. this needs to be used for calculating the final numbers in correct lenghts eg. µm and nm and not "micro millimeter"
## at below which distance in mm should two points regarded as at the same coordinate
max_tolerance = 0.01

## this calculates all distances between each end/start point of the given LINE and ARC
def calculate_distances(line,arc):
    distances = dict()
    distances["line_s_to_arc_s"] = line.dxf.start - arc.start_point
    distances["line_s_to_arc_e"] = line.dxf.start - arc.end_point
    distances["line_e_to_arc_s"] = line.dxf.end   - arc.start_point
    distances["line_e_to_arc_e"] = line.dxf.end   - arc.end_point
    return distances        

doc = ezdxf.readfile(infile)
msp = doc.modelspace()

## some statistics
arcs    = msp.query("ARC")
lines   = msp.query("LINE")
circles = msp.query("CIRCLE")
print("Found "+str(len(arcs))+" arcs, "+str(len(lines))+" lines and "+str(len(circles))+" circles in the drawing.")

corrections = list() ## keep a list of all performed changes
no_of_corrections = 0
for line in lines:
    for arc in arcs:
        distances = calculate_distances (line,arc)
        for dist in distances:
            ## Check if distance for X and Y is below the tolerance. Ignore endpoints that are already at the same location
            if (abs(distances[dist][0]) < max_tolerance) and (abs(distances[dist][1]) < max_tolerance) and not (distances[dist] == (0.0,0.0,0.0)):
                ## correcting the line coordinates to fit onto the arc endpoints
                if dist == "line_s_to_arc_s": ## start of line and start of arc
                    line.dxf.start = arc.start_point
                    corrections.append("Fixed start of "+str(line)+" to start of "+str(arc)
                                       +str(" at "+f"{arc.start_point[0]:.3f}:{arc.start_point[1]:.3f}"+".")
                                       )
                if dist == "line_s_to_arc_e": ## start of line and end of arc
                    line.dxf.start = arc.end_point
                    corrections.append("Fixed start of "+str(line)+" to end of "+str(arc)
                                       +str(" at "+f"{arc.end_point[0]:.3f}:{arc.end_point[1]:.3f}"+".")
                                       )
                if dist == "line_e_to_arc_s": ## end of line and start of arc
                    line.dxf.end = arc.start_point
                    corrections.append("Fixed end of "+str(line)+" to start of "+str(arc)
                                       +str(" at "+f"{arc.start_point[0]:.3f}:{arc.start_point[1]:.3f}"+".")
                                       )
                if dist == "line_e_to_arc_e": ## end of line and end of arc
                    line.dxf.end = arc.end_point
                    corrections.append("Fixed end of "+str(line)+" to end of "+str(arc)
                                       +str(" at "+f"{arc.end_point[0]:.3f}:{arc.end_point[1]:.3f}"+".")
                                       )
                corrections.append(" Diff X = "+str(formatter(distances[dist][0]*base_unit))
                                   +"\n Diff Y = "+str(formatter(distances[dist][1]*base_unit))
                                   +"\n Distance = "+str(formatter(math.sqrt((distances[dist][0]*base_unit)**2+(distances[dist][1]*base_unit)**2)))
                                   )
                no_of_corrections += 1
with open(logfile,"w",encoding="utf8") as logfile:
    print("writing logfile.")
    print("Corrections done:",file=logfile)
    for correction in corrections:
        print(correction,file=logfile)
    print("\nCorrected "+str(no_of_corrections)+" endpoints of lines.",file=logfile)

print("writing output to file:"+outfile)
doc.saveas(outfile)
print("finished")
