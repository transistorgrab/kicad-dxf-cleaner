# Kicad DXF cleaner


## DXF cleaner
This is a tool that takes DXF files (generated by Kicad).
It looks at entpoints of ARCs and LINEs if the endpoints are within a given tolerance.
The endpoints of the LINEs are then placed at the exact same coordinate as the endpoint of the ARC.
ARCs points are not moved – only the connecting LINEs.
CIRCLEs are left alone completely
